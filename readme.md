# Tasks

1.) You have added a feature (multiple commits) on a 'feature-login' branch that you checked out from development and after completing the feature merged the 'feature-login' branch to 'dev'. Now you realize that you don't want to send the feature in this release as it needs some enhancements. Then waht will you do to release the previous version (without the login feature ) also by not losing merged commits of 'feature-login' branch?

2.) you have to add specific commit 'Added tag and notes routes' from 'feature-login' branch in 'tag-apis' branch.

3.) Suppose you added some functionality then commited it and pushed it, with wrong user config( Author name, email ) then how would you fix it with the right config( Author name, email ) without changing the commit date? you can perform operation on 'note-apis' branch.

#### Note :- Any commits pushed after the test deadline is finished will not be considered.